# Name: BaseState
# Description: This is a base state. It contains the most basic variables, functions, and signals
#              for a state machine to be functional. Any custom state that you want to make will
#              inherit from this.
# Usage:
#    Inherit the state:
#        extends BaseState
extends Node
class_name BaseState

signal finished(next_state)

func enter() -> void:
	# Comment this out if you don't want to see this message.
	print("[ENTER] %s" % name)
	return

func exit() -> void:
	# Comment this out if you don't want to see this message.
	print("[EXIT ] %s" % name)
	return

func process(delta : float) -> void:
	return

func physics_process(delta : float) -> void:
	return

func input(event : InputEvent) -> void:
	return
