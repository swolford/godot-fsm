# godot-fsm
A finite state machine module for Godot Engine.

## Usage:
1. Clone this repo into your project folder.
2. In the object you want the State Machine to work with, click '**Add Child Node**' and select `StateMachine`.
3. Add child nodes that extend from `BaseState` to the State Machine node.
4. Assign the initial state to the State Machine node.
5. You're done!
