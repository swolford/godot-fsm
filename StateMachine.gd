# Name: StateMachine
# Description: This is an extremely basic state machine.
#
# Usage:
#    1. Attach the script to an object.
#    2. Add states as children to the state machine object.
#    3. Configure parameters.
extends Node
class_name StateMachine

# The path to the initial state.
export var initial_state : NodePath

# Track the previous and current state.
var previous_state : Object
var current_state : Object

func _ready() -> void:
	connect_signals()
	# If an initial state is defined, enter it.
	# Otherwise look for the first child in the state list and enter that.
	if initial_state:
		change_state(get_node(initial_state))
	else:
		if get_child_count() > 0:
			var state = get_children().front()
			if state.has_method("enter"):
				change_state(get_children().front())

func _process(delta : float) -> void:
	# Process the current state.
	if current_state:
		current_state.process(delta)

func _physics_process(delta : float) -> void:
	# Process the current state.
	if current_state:
		current_state.physics_process(delta)

func _input(event : InputEvent) -> void:
	# Pass input to the current state.
	if current_state:
		current_state.input(event)

# Connects signals.
func connect_signals():
	# We are using the children of this object as our available states.
	for state in get_children():
		# Listen for the finished signal to let us know when to change states.
		state.connect("finished", self, "_on_state_finished")

# Exits the current state, assigns the new state to the current state, and enter it.
func change_state(new_state : Object) -> void:
	if current_state:
		# Prevent entering the same state.
		if current_state == new_state:
			return
		else:
			# Cleanly exit the state and set it as the previous state.
			current_state.exit()
			previous_state = current_state
	# Change the state to the new state and enter it.
	current_state = new_state
	current_state.enter()

# Fired when a state has finished.
func _on_state_finished(next_state : String):
	change_state(get_node(next_state))
